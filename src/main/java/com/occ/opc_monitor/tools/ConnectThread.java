package com.occ.opc_monitor.tools;

import com.occ.opc_monitor.config.opcConfig;
import lombok.SneakyThrows;
import org.openscada.opc.lib.da.AutoReconnectController;

public class ConnectThread extends Thread {
    private opcConfig opcConfig;
    public ConnectThread(opcConfig opcConfig){
        this.opcConfig=opcConfig;
    }
    private boolean alreadyWait=false;
    private boolean alreadyRun=false;

    @SneakyThrows
    @Override
    public void run() {
        AutoReconnectController autoReconnectController=new AutoReconnectController(opcConfig.opc_server(), 5000);
        autoReconnectController.connect();
    }
}
